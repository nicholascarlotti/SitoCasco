<?php

class User{
	
	public function __construct($id, $username, $password, $mail){
		$this->id = $id;
		$this->username = $username;
		$this->password = $password;
		$this->mail = $mail;
	}

	public function getMail(){
		return $this->mail;
	}

	public function getUsername(){
		return $this->username;
	}

	public function getPassword(){
		return $this->password;
	}

	public function setUsername($name){
		$this->name = $name;
	}

	public function setMail($mail){
		$this->mail = $mail;
	}

	public function setPassword($password){
		$this->password = $password;
	}

	public function getID(){
		return $this->id;
	}

	public function setID($id){
		$this->id = $id;
	}

	public function __toString(){
		return "User Object: {id: $this->id, username: $this->username}";
	}
}