<?php
include_once __DIR__."/../DatabaseSetup.php";
include_once __DIR__."/../connectors/DatabaseConnector.php";
include_once __DIR__."/../../model/Device.php";

class DevicesDatabaseAdapter{

	public static function getDeviceBySerial($serial){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$queryResult = $conn->executeQuery("SELECT * FROM Devices WHERE serial_ = ?", [$serial]);
		if(!isset($queryResult[0])){
			return null;
		}
		$device = $queryResult[0];
		return DevicesDatabaseAdapter::deviceFactoryFromQuery($device);
	}

	public static function getDeviceByID($id){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$queryResult = $conn->executeQuery("SELECT * FROM Devices WHERE id = ?", [$id]);
		if(!isset($queryResult[0])){
			return null;
		}
		$device = $queryResult[0];
		return DevicesDatabaseAdapter::deviceFactoryFromQuery($device);
	}

	public static function getDevicesByOwnerID($ownerID){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$devices = $conn->executeQuery("SELECT * FROM Devices WHERE owner_id = ?", [$ownerID]);
		// Rewrite in functional programming style
		$resultDevices = array();
		foreach ($devices as $device) {
			array_push($resultDevices, DevicesDatabaseAdapter::deviceFactoryFromQuery($device));
		}
		return $resultDevices;
	}

	public static function getDevicesByOwnerUsername($ownerUsername){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$devices = $conn->executeQuery("SELECT * FROM Devices WHERE owner_id = (SELECT id FROM Users WHERE username= ?)", [$ownerUsername]);
		$resultDevices = array();
		foreach ($devices as $device) {
			array_push($resultDevices, DevicesDatabaseAdapter::deviceFactoryFromQuery($device));
		}
		return $resultDevices;
	}

	public static function updateDevice($device){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("UPDATE Devices SET serial_ = ?, owner_id = ? WHERE id = ?", 
			[$device->getSerial(), $device->getOwnerID(), $device->getID()]);
		if($result !== 1){
			throw new Exception("Could not update the device $device", 1);
		}
	}

	public static function removeDevice($device){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("DELETE FROM Devices WHERE id = ?", [$device->getID()]);
		if($result !== 1){
			throw new Exception("Could not remove the device $device", 1);
		}
	}

	public static function addDevice($device){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("INSERT INTO Devices (serial_, owner_id, api_key) VALUES (?,?,?)", 
			[$device->getSerial(), $device->getOwnerID(), $device->getAPIKey()]);
		if($result !== 1){
			throw new Exception("Could not add the device $device", 1);
		}
		return $result === 1;
	}

	public static function getDeviceByAPIKey($apiKey){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeQuery("SELECT * FROM Devices WHERE api_key = ?", [$apiKey]);
		if(!isset($result[0])){
			return null;
		}
		$result = $result[0];
		return DevicesDatabaseAdapter::deviceFactoryFromQuery($result);
	}

	public static function deviceFactoryFromQuery($queryResult){
		$device = new Device(
			$queryResult["id"], $queryResult["serial_"], $queryResult["owner_id"], $queryResult['friendly_name'], $queryResult["api_key"]);
		return $device;
	}

}