<?php

class SettingsController{
	
	public function returnJSONSettings(){
		$apiKey = filter_input(INPUT_GET, "api_key");

		if($apiKey === null){
			throw new Exception("Missing api key", 1);
		}

		include_once __DIR__."/../database/adapters/DevicesDatabaseAdapter.php";
		include_once __DIR__."/../database/adapters/SettingsDatabaseAdapter.php";
		include_once __DIR__."/../model/Setting.php";
		include_once __DIR__."/../model/Device.php";

		$device = DevicesDatabaseAdapter::getDeviceByAPIKey($apiKey);
		if($device === null){
			throw new Exception("No device with that api key was found", 1);
		}

		$setting = SettingsDatabaseAdapter::getSettingByDeviceID($device->getID());
		header("Content-Type: application/json;charset=utf-8");
		echo(json_encode($setting));
	}

	public function handleSettingPOST(){
			$apiKey = filter_input(INPUT_POST, "api_key");
			$jsonData = filter_input(INPUT_POST, "settings");

			include_once __DIR__."/../model/Device.php";
			include_once __DIR__."/../database/adapters/DevicesDatabaseAdapter.php";
			$device = DevicesDatabaseAdapter::getDeviceByAPIKey($apiKey);
			if($device === null){
				throw new Exception("Device not found", 1);
			}
			//Check if all the variables were set, throw an exception insead.
			include_once __DIR__."/../model/Setting.php";
			include_once __DIR__."/../database/adapters/SettingsDatabaseAdapter.php";
			$setting = SettingsDatabaseAdapter::getSettingByDeviceID($device->getID());
			$newSetting = Setting::jsonDeserialize($jsonData);
			$newSetting->setDeviceID($device->getID());

			if($setting === null){
				SettingsDatabaseAdapter::addSetting($newSetting, $device->getOwnerID());
			}else{
				$newSetting->setID($setting->getID());
				SettingsDatabaseAdapter::updateSetting($newSetting);
			}

			http_response_code(200);
			die();
	}

	public function run(){
		if($_SERVER["REQUEST_METHOD"] === "POST"){
			$this->handleSettingPOST();

		}elseif($_SERVER["REQUEST_METHOD"] === "GET"){
			$this->returnJSONSettings();
		}
		
	}
}