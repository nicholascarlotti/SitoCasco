<?php

class LogViewController{


	public function viewLogFile(){
		session_start();
		$username = $_SESSION["username"];
		if(!isset($username)){
			throw new Exception("You must be logged in to download a log", 1);
		}

		include_once __DIR__."/../database/adapters/UsersDatabaseAdapter.php";
		include_once __DIR__."/../model/User.php";
		$user = UsersDatabaseAdapter::getUserByName($username);
		if($user === null){
			throw new Exception("This user does not exist", 1);
		}
		$logID = filter_input(INPUT_GET, "log_id");
		if($logID === null){
			throw new Exception("Missing log_id parameter", 1);
		}

		include_once __DIR__."/../database/adapters/LogsDatabaseAdapter.php";
		$log = LogsDatabaseAdapter::getLogById($logID);
		if($log === null){
			throw new Exception("The log with the given id was not found.", 1);
		}
		include_once __DIR__."/../database/adapters/DevicesDatabaseAdapter.php";
		include_once __DIR__."/../model/Device.php";
		$device = DevicesDatabaseAdapter::getDeviceByID($log->getDeviceID());

		if($device === null || $device->getOwnerID() !== $user->getID()){
			throw new Exception("Could not access the log", 1);
		}
		$contentLength = strlen($log->getData());
		$filename = $log->getSyncDate()->format("Y-m-d H:i:s");
		header("Content-length: $contentLength");
		header("Content-type: text/plain");

		if(filter_input(INPUT_GET, "download") !== null){
			header("Content-Disposition: attachment; filename=log-$filename");
		}

		echo $log->getData();
	}

	public function viewLogMetadata(){
		session_start();
		$username = $_SESSION["username"];
		if(!isset($username)){
			throw new Exception("You must be logged in to access this information", 1);
		}

		include_once __DIR__."/../database/adapters/UsersDatabaseAdapter.php";
		include_once __DIR__."/../model/User.php";
		$user = UsersDatabaseAdapter::getUserByName($username);
		if($user === null){
			throw new Exception("This user does not exist", 1);
		}
		include_once __DIR__."/../database/adapters/LogsDatabaseAdapter.php";
		
		$logsByDate = array();
		$data = array();
		$dates = array();
		$ids = array();
		$logSizes = array();
		$deviceNames = array();
		foreach (LogsDatabaseAdapter::getLogsMetadataByUser($user) as $value) {
			array_push($data, $value[1]);
			array_push($dates, $value[0]);
			array_push($ids, $value[2]);
			array_push($logSizes, $value[3]);
			array_push($deviceNames, $value[4]);
		}
		$logsByDate["data"] = $data;
		$logsByDate["dates"] = $dates;
		$logsByDate["ids"] = $ids;
		$logsByDate["sizes"] = $logSizes;
		$logsByDate["device_names"] = $deviceNames;
		print(json_encode($logsByDate));
	}

	public function run(){
		
		if($_SERVER["REQUEST_METHOD"] === "GET"){
			if(filter_input(INPUT_GET, "metadata") !== null){
				$this->viewLogMetadata();
			}else{
				$this->viewLogFile();
			}
		}
	}
}