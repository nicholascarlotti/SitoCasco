<?php

use PHPUnit\Framework\TestCase;

include_once __DIR__."/../../../src/model/User.php";
include_once __DIR__."/../../../src/database/adapters/UsersDatabaseAdapter.php";
include_once __DIR__."/../../../src/database/connectors/SQLiteConnector.php";

final class UserDatabaseAdapterTest extends TestCase{

	/**
	 * @before
	 */
	public function setUp(){
		$this->conn = new SQLiteConnector("");
		$this->conn->executeDMLQuery("create table Users(
				id integer primary key autoincrement,
				username varchar(30) not null,
				pwd varchar(30) not null,
				mail varchar(250) not null
			)");
	}

	/**
	 * @after
	 *
	 */
	public function tearDown(){
		$this->conn->close();
	}


	public function testGetUserFromDatabase(){
		$user = new User(null, "testuser", "123456", "test@testmail.com");
		$this->conn->executeDMLQuery("INSERT INTO Users(username, pwd, mail) VALUES(?,?,?)",
			[$user->getUsername(), $user->getPassword(), $user->getMail()]);
		$otherUser = UsersDatabaseAdapter::getUserByName("testuser");
		$otherUser->setID(null); // We don't care about the id for now
		$this->assertEquals($user, $otherUser);

		$otherUser = UsersDatabaseAdapter::getUserByName("nonexistent");
		$this->assertNull($otherUser);
	}

	public function testPutUserInDatabase(){
		$user = new User(null, "testuser", "123456", "test@testmail.com");
		$res = UsersDatabaseAdapter::addUserToDatabase($user);
		$this->assertEquals(true, $res);
		$otherUser = UsersDatabaseAdapter::getUserByName("testuser");
		$otherUser->setID(null);
		$this->assertEquals($user, $otherUser);
	}

	public function testDeleteUserInDatabase(){
		$user = new User(null, "testuser", "123456", "test@testmail.com");
		$res = UsersDatabaseAdapter::addUserToDatabase($user);
		$this->assertEquals(true, $res);
		$user = UsersDatabaseAdapter::getUserByName("testuser");
		$res = UsersDatabaseAdapter::removeUserFromDatabase($user);
		$this->assertEquals(true, $res);
		$user = UsersDatabaseAdapter::getUserByName("testuser");
		$this->assertNull($user);
	}	
}