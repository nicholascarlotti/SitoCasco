<?php

use PHPUnit\Framework\TestCase;
include_once __DIR__."/../../../src/database/connectors/SQLiteConnector.php";

final class SQLiteConnectorTest extends TestCase{

	public function testDatabaseCreation(){
		$conn = new SQLiteConnector("");
		$conn->executeDMLQuery("CREATE TABLE test(a integer primary key, b varchar(30) not null)");
		$res = $conn->executeDMLQuery("INSERT INTO test(a, b) VALUES (?, ?)", [1, "abc"]);
		$this->assertEquals(1, $res, "Could not add a row into temporary database");
	}

	public function testDatabaseQuery(){
		$conn = new SQLiteConnector("");
		$conn->executeDMLQuery("CREATE TABLE test(a integer primary key, b varchar(30) not null)");
		$conn->executeDMLQuery("INSERT INTO test(a, b) VALUES (?, ?)", [1, "abc"]);
		$res = $conn->executeQuery("SELECT * FROM test")[0];
		$this->assertEquals("1", $res["a"], "The retrieved row does not match the expected one");
		$this->assertEquals("abc", $res["b"], "The retrieved row does not match the expected one");
	}
}